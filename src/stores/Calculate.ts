import { ref } from "vue";
import { defineStore } from "pinia";

export const useCalStore = defineStore("Calculate", () => {
  const output = ref(0);

  const increment = () => {
    if (output.value >= 0) {
      output.value++;
    }
  };

  const decrease = () => {
    if (output.value > 0) {
      output.value--;
    }
  };

  return {
    output,
    increment,
    decrease,
  };
});
