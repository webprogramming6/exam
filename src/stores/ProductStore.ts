import { ref } from "vue";
import { defineStore } from "pinia";
import type Product from "./Product";

export const useProductStore = defineStore("product", () => {
  const dialog = ref(false);
  const isTable = ref(true);
  const edited = ref<Product>({ id: -1, name: "", price: 0 });
  let lastId = 0;
  const products = ref<Product[]>([]);
  const sum = ref(0);

  const saveProduct = (id: number, name: string, price: number) => {
    edited.value.id = id;
    edited.value.name = name;
    edited.value.price = price;
    lastId = lastId++;
    products.value.push(edited.value);
    sum.value = sum.value + price;
    clear();
  };

  const clear = () => {
    edited.value = { id: -1, name: "", price: 0 };
  };
  return {
    products,
    dialog,
    edited,
    clear,
    saveProduct,
    lastId,
    isTable,
    sum,
  };
});
